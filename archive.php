<?php
/**
* The template for displaying archive pages
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package Theledger
*/

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

  <?php if ( have_posts() ) : ?>

    <header class="container page-header">
      <div class="row justify-content-center">
        <div class="col-md-9">
          <h1 class="page-title text--md"><?php echo get_the_archive_title() ?></h1>
          <?php
              the_archive_description( '<div class="archive-description">', '</div>' );
            ?>
        </div>
      </div>
    </header><!-- .page-header -->

    <div class="container">
      <?php
        /* Start the Loop */
        while ( have_posts() ) :
          the_post();

          /*
          * Include the Post-Type-specific template for the content.
          * If you want to override this in a child theme, then include a file
          * called content-___.php (where ___ is the Post Type name) and that will be used instead.
          */
          get_template_part( 'template-parts/content_preview', get_post_type() );

        endwhile;
      endif;
      ?>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->

  <?php
  get_sidebar();
  get_footer();
