<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head>
section and everything up until
<div id="content">
  * * @link
  https://developer.wordpress.org/themes/basics/template-files/#template-partials
  * * @package Theledger */ ?>
  <!DOCTYPE html>
  <html <?php language_attributes(); ?>
    >
    <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="profile" href="https://gmpg.org/xfn/11" />
      <link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png"/>
      <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>



      <header class="header container">
        <nav class="navbar navbar-expand-lg navbar-light">
          <?php
            if(!is_front_page()) {
              ?>
              <a class="link-logo" href="/" alt="Torna alla Homepage <--"><h1 class="logo">Theledger</h1></a>
              <!-- <a class="link-logo" href="/" alt="Torna alla Homepage <--"><h1 class="logo">Theledger <span class="link-logo__helper">Torna alla homepage.</span></h1></a> -->
              <?php
            }
          ?>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navcollapse" aria-controls="navcollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navcollapse">
            <?php
              if(!is_front_page()) {
                wp_nav_menu(
                  array(
                  'menu'  => 'header',
                  'container'       => 'div',
                  'menu_class'      => 'nav-header',
                  'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                  'walker'          => new WP_Bootstrap_Navwalker(),
                  )
                );
              } else {
                wp_nav_menu(
                  array(
                  'menu'  => 'home',
                  'container'       => 'div',
                  'menu_class'      => 'nav-header nav-header__home',
                  'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                  'walker'          => new WP_Bootstrap_Navwalker(),
                  )
                );
              }
            ?>
          </div>
        </nav>
      </header>

