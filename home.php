<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Theledger
 */

get_header();
?>
<!-- <div class="fixed-cta">
  <a href="#" class="btn btn-primary">
    Iscriviti ora
  </a>
</div> -->
  <section class="homepage container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-6">
            <h1 class="logo">Theledger</h1>
            <h2 class="title text--xl">Rimani aggiornato sull’evoluzione dell’ ecosistema <span class="rainbow">Ethereum.</span> <br />In Italiano.</h2>
            <ul class="features">
              <li class="feature-item">
                <img
                  class="icon mailbox"
                  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-mailbox.svg"
                  alt="Inviaci una email"
              />
                <p><span class="text-heading weight-bold">Ricevi comodamente</span> la newsletter il Lunedì mattina con l’aggiornamento settimanale sull’ecosistema.</p>
              </li>
              <li class="feature-item">
                <img
                  class="icon time"
                  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-time.svg"
                  alt="Inviaci una email"
              />
                <p><span class="text-heading weight-bold">Guadagna tempo e conoscenza</span> attraverso un’accurata selezione di tematiche calde del momento, insights, report ed analisi di mercato.</p>
              </li>
              <li class="feature-item">
                <img
                  class="icon idea"
                  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-idea.svg"
                  alt="Inviaci una email"
              />
                <p><span class="text-heading weight-bold">Senza distrazioni</span> grazie ad un unico canale. Rimani concentrato su ciò che conta. L’archivio delle mail sarà presente anche sul sito.</p>
              </li>
            </ul>
          </div>
          <div class="col-lg-5 offset-lg-1 ">
            <div class="boxemail">
              <?php
                $form_widget = new \MailPoet\Form\Widget();
                echo $form_widget->widget(array('form' => 1, 'form_type' => 'php'));
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-box">
          <div class="row">
            <div class="col-md-5 illustration-box">
              <img
                  class="illustration rocket"
                  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/rocket.svg"
                  alt="Inviaci una email"
              />
              <img
                  class="illustration book"
                  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/book.svg"
                  alt="Inviaci una email"
              />
            </div>
            <div class="col-md-7">
              <h3 class="text--md">Contenuti esclusivi</h3>
              <p class="text-p--xl mt-3">A partire da 9,99€ al mese puoi accedere ad articoli, studi di mercato e molto altro. Esegui l’upgrade della mail quando desideri.</p>
              <a class="btn btn-green mt-3" href="/acquista/abbonamento">Abbonati</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<?php

get_footer();
