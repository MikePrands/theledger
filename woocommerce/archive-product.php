<?php
/**
* The Template for displaying product archives, including the main shop page which is a post type archive
*
* This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.4.0
*/

defined( 'ABSPATH' ) || exit;


get_header( 'shop' );
?>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-10">
      <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
      <h2>3 semplici abbonamenti a portata di tutti.</h2>
        <div class="product-table">
          <?php
            $args = array(
              'post_type' => 'product',
              'posts_per_page' => 3,
              'orderby' => 'ID',
              'order'   => 'ASC',
              );
            $loop = new WP_Query( $args );
            if ( $loop->have_posts() ) {
              while ( $loop->have_posts() ) : $loop->the_post();
                $product = wc_get_product( $loop->get_id() );
                ?>
                <div class="product-table__item">
                  <h3 class="product-table__title"><?php echo $product->get_title(); ?></h3>
                  <p class="product-table__price"><?php echo $product->get_price(); ?> €</p>
                  <div class="product-table__priceinfo"><?php echo $product->get_short_description(); ?></div>
                  <div class="product-table__pricedesc"><?php echo $product->get_description(); ?></div>
                  <a href="?add-to-cart=<?php echo $product->get_id();?>" data-quantity="1" class="btn btn-primary product_type_subscription add_to_cart_button ajax_add_to_cart product-table__cta" data-product_id="198" data-product_sku="" aria-label="Aggiungi “Mensile” al tuo carrello" rel="nofollow">Sottoscrivi</a>
                </div>
              <?php
              endwhile;
            }

            wp_reset_postdata();
          ?>
        </div>
    </div>
  </div>
</div>
<?php
get_footer( 'shop' );