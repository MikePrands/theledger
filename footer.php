<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Theledger
 */

?>



<?php wp_footer(); ?>
    <section class="s-footer">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="row box-staff justify-content-center">
              <div class="col-12 col-md-4 illustration-box">
                <img
                  class="illustration team"
                  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/team.png"
                  alt="Inviaci una email"
                />
              </div>
              <div class="col-10 col-md-7 offset-md-1">
                <h3 class="text--md mt-6">Uno staff qualificato.</h3>
                <p class="text-p--xl mt-3">Dal 2014 dedichiamo il nostro tempo a fornire contenuti di qualità sull’ecosistema Ethereum.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center mt-5">
          <div class="col-md-12">
            <footer class="footer">
              <p><span class="text-heading text--logo-small ">Theledger</span> <span class="text--copy ml-2">è un progetto <a class="link-nostyle" href="https://ethereum-italia.it/" target="_blank" rel="nofollow noopener noreferrer">Ethereum Italia</a></span></p>
              <ul class="social-nav">
                <li class="social-nav__item">
                  <a class="social-nav__link" href="https://t.me/theledger" target="_blank" rel="nofollow noopener noreferrer">
                    <img class="ico" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-telegram.svg" alt="" />
                  </a>
                </li>
                <li class="social-nav__item">
                  <a class="social-nav__link" href="https://www.facebook.com/theledgeritalia/" target="_blank" rel="nofollow noopener noreferrer">
                    <img class="ico" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-facebook.svg" alt="" />
                  </a>
                </li>
                <li class="social-nav__item">
                  <a  class="social-nav__link" href="https://www.instagram.com/theledgeritalia/" target="_blank" rel="nofollow noopener noreferrer">
                    <img class="ico" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-instagram.svg" alt="" />
                  </a>
                </li>
                <li class="social-nav__item">
                  <a class="social-nav__link" href="https://twitter.com/theledgeritalia" target="_blank" rel="nofollow noopener noreferrer">
                    <img class="ico" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-twitter.svg" alt="" />
                  </a>
                </li>
              </ul>
              <?php
                wp_nav_menu(
                  array(
                    'theme_location'  => 'footer',
                    'container'       => 'div',
                    'menu_class'      => 'nav-footer',
                    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'          => new WP_Bootstrap_Navwalker(),
                  )
                );
              ?>
              <p class="copyright">
              © 2020 Theledger.it - NewCycleInternational Ltd
              </p>

              <p class="disclaimer">Questo sito non rappresenta una testata giornalistica in quanto non viene aggiornato con cadenza periodica né è da considerarsi un mezzo di informazione o un prodotto editoriale ai sensi della legge n.62/2001. Ai sensi e agli effetti dell’art. 187 ter della legge Draghi si precisa ad ogni effetto di legge che l’autore del presente articolo non è iscritto all’Ordine dei Giornalisti e che pertanto detiene, o potrebbe detenere, i valori mobiliari oggetto delle sue analisi. Inoltre questo sito, ed i suoi contenuti, non costituiscono un servizio di consulenza finanziaria o un consiglio operativo, né costituiscono sollecitazione al pubblico risparmio, o a qualsivoglia forma di investimento. I risultati presentati non costituiscono alcuna garanzia relativamente ad ipotetiche performance future. Il sito ed i suoi contenuti hanno scopo puramente didattico, e chi scrive, nonostante abbia messo la massima cura nell’elaborazione dei dati e dei testi, declina ogni responsabilità su eventuali inesattezze dei dati riportati e chiunque investa i propri risparmi prendendo spunto dalle indicazioni riportate, lo fa a proprio rischio e pericolo. Si sottolinea inoltre che l’attività speculativa di trading e di investimento comporta notevoli rischi economici e pertanto il lettore è l’unico ed esclusivo responsabile di ogni sua decisione operativa.
Le opinioni espresse non costituiscono dunque, in alcun modo, consulenza finanziaria o sollecitazione del pubblico risparmio. Non si garantisce l’accuratezza e completezza delle informazioni in esso contenute e si declina ogni responsabilità per gli eventuali usi che gli utenti ne possano fare. Ogni lettore deve considerarsi responsabile per i rischi dei propri investimenti e per l’uso che fa delle informazioni contenute in queste pagine. I consigli non sono un’offerta o un invito a comprare o a vendere titoli. I titoli potrebbero scendere. Il blog viene aggiornato senza alcuna periodicità. Non è una testata giornalistica nè può essere considerato un prodotto editoriale ai sensi della legge n. 62 del 07/ 03/ 2001.
Infine si sottolinea come i diritti di riproduzione, parziale o totale, dei contenuti del sito siano concessi solo dietro permesso scritto dell’autore e con espressa citazione della fonte.</p>
            </footer>
          </div>
        </div>
      </div>
    </section>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>  </body>
</html>
