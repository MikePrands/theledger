<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Theledger
 */

?>

<div class="row justify-content-center post-archive">
  <div class="col-md-9">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <header class="entry-header">
        <?php
        if ( is_singular() ) :
          the_title( '<h1 class="entry-title">', '</h1>' );
        else :
          the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;
      ?>
      </header><!-- .entry-header -->

      <?php theledger_post_thumbnail(); ?>

      <div class="entry-content">
        <?php
          the_content('<a class="cta-more" href="'.get_the_permalink().'" title="'.get_the_title().'">Continua a leggere</a>');
        ?>
      </div><!-- .entry-content -->

    </article><!-- #post-<?php the_ID(); ?> -->
  </div>
</div>

